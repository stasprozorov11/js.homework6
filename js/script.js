// Завдання
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:

// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

function createNewUser() {
  let firstName = prompt("Enter your first name");
  while (!(firstName && /^[a-zA-Z]+$/.test(firstName))) {
    firstName = prompt("Re-enter your first name");
  }

  let lastName = prompt("Enter your last name");
  while (!(lastName && /^[a-zA-Z]+$/.test(lastName))) {
    lastName = prompt("Re-enter your last name");
  }
  let birthday = prompt("Enter you birthday", `dd.mm.yyyy`);

  let today = new Date();

  let newUser = {
    firstName,
    lastName,
    birthday,
  };

  newUser.getLogin = function () {
    return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
  };
  newUser.getAge = function () {
    const birthdate = new Date(birthday.split(".").reverse().join("-"));
    let age = today.getFullYear() - birthdate.getFullYear();
    let month = today.getMonth() - birthdate.getMonth();
    if (month < 0 || (month === 0 && today.getDate() < birthdate.getDate()))
      age--;

    return age;
  };

  newUser.getPassword = function () {
    return `${
      newUser.firstName.slice(0, 1).toUpperCase() +
      newUser.lastName.toLowerCase() +
      newUser.birthday.slice(6)
    }`;
  };

  return newUser;
}

let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
